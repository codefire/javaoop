/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.second;

/**
 *
 * @author CodeFire
 */
public class SimpleWorker {

    private String name;
    private double salary;

    public String getWorkerName() {
        return name;
    }
    
    public String getName() {
        return name;
    }

    /**
     * This method is <b>DEPRECATED</b>
     * Use new method setName(String name)
     * @param workerName
     * @deprecated
     */
    @Deprecated
    public void setWorkerName(String workerName) {
        this.setName(name);
    }
    
    public void setName(String name){
        if(name == null || name.trim().isEmpty()){
            name = "John Doe";
        }
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getInfo() {
        return "Worker {name: " + name + ", salary: " + salary + "}";
    }
}
