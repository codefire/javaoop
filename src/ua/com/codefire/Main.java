/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire;

import ua.com.codefire.example.Counter;
import ua.com.codefire.example.Driver;
import ua.com.codefire.example.Manager;
import ua.com.codefire.example.Worker;
import ua.com.codefire.first.Book;

/**
 *
 * @author codefire
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        Book first = new Book("JavaSE", "Oracle", 850);
        Book second = new Book("JavaSE", "Oracle", 850);
        System.out.println("Same: " + (first == second));
        System.out.println("Equals: " + first.equals(second));
        System.out.println("First hash: " + first.hashCode());
        System.out.println("Second hash: " + second.hashCode());
        */

        /*
        Worker first = new Worker();
        first.setWorkerName("Ivan");
        first.setSalary(2500.0);
        System.out.println(first.getInfo());
        */
        Manager manager = new Manager();
        manager.setName("Ivan");
        manager.setSalary(2500.0);
        manager.setSales(55_000.0);

        Driver driver = new Driver();
        driver.setName("Petr");
        driver.setSalary(2350.0);
        driver.setOwner(true);

        Counter counter = new Counter();
        counter.setName("Sidor");
        counter.setSalary(5500.0);
        counter.setAssist(false);

        double budget = 0;
        Worker[] workers = {manager, driver, counter};
        for (Worker worker : workers) {
            budget += worker.getSalary();
            System.out.println(worker.getInfo());
        }
        System.out.println("Budget: " + budget);
    }

}
