/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.example;

/**
 *
 * @author CodeFire
 */
public class Driver extends Worker{

    private boolean owner;

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    @Override
    public String getInfo(){
        return "Driver {name: " + getName() + ", salary: " + getSalary() + ", owner: " + owner + "}";
    }
}
