/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.example;

/**
 *
 * @author CodeFire
 */
public class Counter extends Worker {

    private boolean assist;

    public boolean isAssist() {
        return assist;
    }

    public void setAssist(boolean assist) {
        this.assist = assist;
    }
    
    @Override
    public String getInfo(){
        return "Counter {name: " + getName() + ", salary: " + getSalary() + ", assist: " + assist + "}";
    }

}
