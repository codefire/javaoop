/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.example;

/**
 *
 * @author CodeFire
 */
public class Manager extends Worker{

    private double sales;

    public double getSales() {
        return sales;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }
    
    @Override
    public String getInfo(){
        return "Manager {name: " + getName() + ", salary: " + getSalary() + ", sales: " + sales + "}";
    }

}
