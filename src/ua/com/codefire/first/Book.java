/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.first;

import java.io.Serializable;
import java.util.Objects;

/**
 * JavaBean
 *
 * @author codefire
 */
public class Book implements Serializable {

    // 1. fields
    private String name;
    private String author;
    private int pages;

    // 2. constructors
    public Book() {
    }

    public Book(String name, String author, int pages) {
        this.name = name;
        this.author = author;
        this.pages = pages;
    }

    public Book(String name, int pages) {
        /*
         this.name = name;
         this.author = "People";
         this.pages = pages;
         */
        this(name, "People", pages);
    }

    // 3. properties
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.author);
        hash = 97 * hash + this.pages;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        return this.pages == other.pages;
    }

    @Override
    public String toString() {
        return "Book{" + "name=" + name + ", author=" + author + ", pages=" + pages + '}';
    }

}
